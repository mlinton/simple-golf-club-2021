<?php
/**
 * Get the players for the current team and display them as a table.
 */
if( array_key_exists('status', $event_list) && 'success' === $event_list['status'] ) :
    $player_list = sgc_team_getplayers();
    if( 'success' === $player_list['status'] ) :
    ?>
        <div class="sgc-2021-team-players">
            <h4><?php _e('Player Roster', 'simple-golf-club-2021') ?></h4>
            <table>
                <tbody>
                    <?php foreach( $player_list['data'] as $player ) : ?>
                    <tr>
                        <td class="name"><a href="<?php echo esc_url($player['URL']) ?>" target="_blank"><?php echo esc_html($player['name']) ?></a></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
<?php endif;
