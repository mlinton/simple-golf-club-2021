<?php
/**
 * Get the player info for the current player and display in a table.
 */
if (function_exists('sgc_player_getinfo')) :
    $player_info = sgc_player_getinfo();
if( array_key_exists('status', $player_info) && 'success' === $player_info['status'] ) :
    ?>
        <div class="sgc-2021-player-info">
            <h4><a href="<?php echo esc_url($player_info['data']['URL']) ?>" target="_blank"><?php echo esc_html($player_info['data']['name']) ?></a></h4>
            <table>
                <tbody>
                    <?php if( !empty($player_info['data']['phone']) ) : ?>
                    <tr>
                        <th><?php _e('Phone', 'simple-golf-club-2021') ?></th>
                        <td class="phone"><a href="phone:<?php echo esc_attr($player_info['data']['phone']) ?>" target="_blank">
                            <?php echo esc_html($player_info['data']['phone']) ?></a></td>
                    </tr>
                    <?php endif; ?>
                    <?php if( !empty($player_info['data']['email']) ) : ?>
                    <tr>
                        <th><?php _e('Email', 'simple-golf-club-2021') ?></th>
                        <td class="email"><a href="mailto:<?php echo esc_attr($player_info['data']['email']) ?>" target="_blank">
                            <?php echo esc_html($player_info['data']['email']) ?></a></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
<?php endif;
