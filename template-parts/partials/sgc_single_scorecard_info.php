<?php
/**
 * Template part for displaying sgc_scorecard
 * @since simple-golf-club-2021 1.0
 */

?>

<?php
if (function_exists('sgc_scorecard_getinfo')) :
    $scorecard = sgc_scorecard_getinfo();
    if( array_key_exists('status', $scorecard) && 'success' === $scorecard['status'] ) : 
        $total_strokes = 0;
    ?>
        <div class="sgc-2021-scorecard">
            <div class="header">
                <a href="<?php echo esc_html($scorecard['data']['player_url']) ?>"><h3 class="player"><?php echo esc_html($scorecard['data']['player_name']) ?></h3></a>
                <a href="<?php echo esc_html($scorecard['data']['event_url']) ?>"><h4 class="event"><?php echo esc_html($scorecard['data']['event_name']) ?></h4></a>
                <span class="tee"><?php echo esc_html($scorecard['data']['tee_color']) ?> (<?php echo esc_html($scorecard['data']['tee_difficulty']) ?>)</span>
            </div>
            
            <div class="content">
                 <div class="tees">
                    <div class="frontnine">
                        <h4><?php _e('Front Nine', 'simple-golf-club-2021') ?></h4>
                        <table>
                            <tbody>
                                <tr class="hole">
                                    <th><?php _e('Hole', 'simple-golf-club-2021') ?></th>
                                    <?php for ($i = 1; $i <= 9; $i++): ?>
                                        <th><?php echo $i ?></th>
                                    <?php endfor; ?>
                                </tr>
                                <tr class="strokes">
                                    <th><?php _e('Strokes', 'simple-golf-club-2021') ?></th>
                                    <?php for ($i = 0; $i < 9; $i++): ?>
                                        <td><?php echo esc_html($scorecard['data']['strokes'][$i]) ?></td>
                                        <?php $total_strokes += $scorecard['data']['strokes'][$i] ?>
                                    <?php endfor; ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="backnine">
                        <h4><?php _e('Back Nine', 'simple-golf-club-2021') ?></h4>
                        <table>
                            <tbody>
                                <tr class="hole">
                                    <th><?php _e('Hole', 'simple-golf-club-2021') ?></th>
                                    <?php for ($i = 10; $i <= 18; $i++): ?>
                                        <th><?php echo $i ?></th>
                                    <?php endfor; ?>
                                </tr>
                                <tr class="strokes">
                                    <th><?php _e('Strokes', 'simple-golf-club-2021') ?></th>
                                    <?php for ($i = 9; $i < 18; $i++): ?>
                                        <td><?php echo esc_html($scorecard['data']['strokes'][$i]) ?></td>
                                        <?php $total_strokes += $scorecard['data']['strokes'][$i] ?>
                                    <?php endfor; ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="stats">
                    <span class="greens"><?php _e('Greens', 'simple-golf-club-2021') ?>: <?php echo esc_html($scorecard['data']['greens']) ?></span>
                    <span class="fairways"><?php _e('Fairways', 'simple-golf-club-2021') ?>: <?php echo esc_html($scorecard['data']['fairways']) ?></span>
                    <span class="putts"><?php _e('Putts', 'simple-golf-club-2021') ?>: <?php echo esc_html($scorecard['data']['putts']) ?></span>
                    <span class="total"><?php _e('Total', 'simple-golf-club-2021') ?>: <?php echo esc_html($total_strokes) ?></span>
                </div>
                
            </div>
        </div>
    <?php else: ?>
        <div class="sgc-2021-scorecard no-scorecard">
            <h4><?php _e('Scorecard', 'simple-golf-club-2021') ?></h4>
            <p><?php _e('Scorecard has not been created yet.', 'simple-golf-club-2021') ?></p>
        </div>
    <?php endif; ?>
<?php endif; ?>
