<?php
/**
 * Get the event info for the current event and display in a table.
 */
if (function_exists('sgc_event_getinfo')) :
    $event_info = sgc_event_getinfo();
    if( array_key_exists('status', $event_info) && 'success' === $event_info['status'] ) :
    ?>
        <div class="sgc-2021-event-info">
            <h4><?php _e('Event Info', 'simple-golf-club-2021') ?></h4>
            <table>
                <tbody>
                    <tr>
                        <td>
                            <span class="time"><?php echo date( 'g:i A', strtotime($event_info['data']['time']) ) ?></span>
                            <span class="date"><?php echo date( 'M d, Y', strtotime($event_info['data']['time']) ) ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="location-name"><a href="<?php echo esc_url($event_info['data']['location_url']) ?>" target="_blank">
                            <?php echo esc_html($event_info['data']['location_name']) ?></a></td>
                    </tr>
                    <?php if( !empty($event_info['data']['team_name']) ) : ?>
                    <tr>
                        <td class="team-name"><a href="<?php echo esc_url($event_info['data']['team_url']) ?>" target="_blank">
                            <?php echo esc_html($event_info['data']['team_name']) ?></a></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
<?php endif;
